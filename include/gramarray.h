#ifndef GRAMARRAY_H
#define GRAMARRAY_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef intptr_t gramarray;
typedef intptr_t gramiter;
typedef int64_t  gramtype;
typedef void (gramcallback)(int64_t, void*);

enum {
	GRAMARRAY_ABSOLUTE = 0,
	GRAMARRAY_RELATIVE
};

enum {
	GRAMARRAY_FORWARD  = 1,
	GRAMARRAY_BACKWARD = -1
};

/**
 * Creates a gramarray. The result will be stored in g.
 * type should be either GRAMARRAY_ABSOLUTE or GRAMARRAY_RELATIVE.
 * GRAMARRAY_ABSOLUTE indicates that values will be stored "as is".
 * GRAMARRAY_RELATIVE indicates that differences between consecutive
 * values are stored.
 * Returns 0 on success, -1 on failure.
 */
int gramarray_create(gramarray* g, int type);

/**
 * Frees the gramarray.
 * Returns 0 on success, -1 on failure.
 */
int gramarray_free(gramarray g);

/**
 * Appends an item at the end of the stream.
 * This will invalidate all the iterators that have been built before.
 * Returns 0 on success, -1 on failure.
 */
int gramarray_append(gramarray g, gramtype x);

/**
 * Creates an iterator pointing to the beginning of the array.
 * direction = GRAMARRAY_FORWARD or GRAMARRAY_BACKWARD.
 * Returns 0 on success, -1 on failure.
 */
int gramarray_iterator_create(gramarray g, gramiter* it, int direction);

/**
 * Frees an iterator.
 * Returns 0 on success, -1 on failure.
 */
int gramarray_iterator_free(gramiter it);

/**
 * Get the current value pointed by the iterator.
 * Returns 0 on success, -1 if the iterator is invalid.
 */
int gramarray_iterator_value(gramiter it, gramtype* val);

/**
 * Increments or decrement the iterator (depending on whether it's
 * a forward or backward iterator).
 * Returns 0 on success, -1 if the iterator is invalid
 * or is already at the end of the grammar and cannot be incremented.
 */
int gramarray_iterator_next(gramiter it);

/**
 * Iterates a callback function over the grammar.
 * Returns 0 on success, -1 on failure.
 */
int gramarray_iterate(gramarray g, gramcallback cb, int direction, void* args);

/**
 * Prints a representation of the internal grammar.
 */
int gramarray_print(FILE* file, gramarray g);

#ifdef __cplusplus
}
#endif
#endif
