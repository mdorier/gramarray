set(SOURCES ${GRAMARRAY_SOURCE_DIR}/src/gramarray.c
            ${GRAMARRAY_SOURCE_DIR}/src/pool.c)

add_library(gramarray ${SOURCES})
