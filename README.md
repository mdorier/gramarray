# README #

Gramarray is a C library designed to compress arrays of 64-bit signed integers using the StarSequitur algorithm, an algorithm improving on the Sequitur algorithm.

### Installing ###

    cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX:PATH=/path/to/install
    make
    make install

### Using ###

An example is provided in *examples/test.c*.