#include "pool.h"
#include <stdlib.h>
#include <stdio.h>

#define ASSERT(cond,msg) if(!(cond)) { fprintf(stderr,msg); exit(-1); }

typedef unsigned char uchar;

pool_entry* pool_entry_create(uchar size, uint32_t item_size) {
	pool_entry* e = (pool_entry*)malloc(sizeof(pool_entry));
	ASSERT(e,"Memory allocation failed in pool (entry section)\n");
	e->data = (char*)malloc(item_size*size);
	ASSERT(e->data,"Memory allocation failed in pool (data section)\n");
	e->holes.top = 0;
	e->holes.content = (uchar*)malloc(size*sizeof(uchar));
	ASSERT(e->holes.content,"Memory allocation failed in pool (metadata section)\n");
	e->next = e;
	e->prev = e;
	int i;
	for(i=0; i<size;i++) e->holes.content[i] = i;
	return e;
}

void pool_entry_free(pool_entry* e) {
	ASSERT(e,"Trying to free a NULL pool entry\n");
	e->next->prev = e->prev;
	e->prev->next = e->next;
	free(e->holes.content);
	free(e->data);
	free(e);
}

void* pool_alloc_item(pool* p) {
	ASSERT(p,"Trying to allocate an item in a NULL pool");
	if(p->entry == NULL) {
		p->entry = pool_entry_create(p->chunk_size,p->item_size);
	}
	// find an entry that has space
	pool_entry* e = p->entry;
	while(e->holes.top == p->chunk_size) {
		e = e->next;
		if(e == p->entry) break;
	}
	// if none of the entries have space, create new entry
	if(e->holes.top == p->chunk_size) {
		e = pool_entry_create(p->chunk_size,p->item_size);
		pool_entry* a = p->entry;
		pool_entry* b = p->entry->prev;
		e->next = a;
		e->prev = b;
		a->prev = e;
		b->next = e;
		p->entry = e;
	}
	// here we have an entry e that has space
	uchar f = e->holes.content[e->holes.top];
	e->holes.top++;
	return (void*)(e->data + (p->item_size*f));
}

void pool_free_item(pool* p, void* ptr) {
	ASSERT(p,"Trying to free item from a NULL pool\n");
	intptr_t c = (intptr_t)ptr;
	pool_entry* e = p->entry;
	pool_entry* last = p->entry->prev;
	intptr_t pos = -1;
	while(pos < 0 || pos >= p->chunk_size) {
		pos = c - (intptr_t)(e->data);
		pos /= p->item_size;
		if(pos >= 0 && pos < p->chunk_size) break;
		if(e == last) break;
		e = e->next;
	}
	if(pos >= 0 && pos < p->chunk_size) {
		e->holes.top--;
		e->holes.content[e->holes.top] = (uchar)pos;
	}
	ASSERT((pos >= 0 && pos < p->chunk_size), "Pointer does not belong to the pool\n");
	if(e->holes.top == 0) {
		if(p->entry == e) p->entry = e->next;
		if(p->entry->prev == p->entry) p->entry = NULL;
		pool_entry_free(e);
	}
}
