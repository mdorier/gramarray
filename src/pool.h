#ifndef POOL_H
#define POOL_H

#include <stdint.h>

typedef struct pool pool;
typedef struct pool_entry pool_entry;

struct pool_entry {
	char* data;
	unsigned char pos;
	struct {
		unsigned int top;
		unsigned char* content;
	} holes;
	pool_entry* next;
	pool_entry* prev;
};

struct pool {
	unsigned char chunk_size;
	unsigned int item_size;
	pool_entry* entry;
};

pool_entry* pool_entry_create(unsigned char size, uint32_t item_size);
void  pool_entry_free(pool_entry* p);

void* pool_alloc_item(pool* p);
void  pool_free_item(pool* p, void* ptr);

#endif
