#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "uthash.h"
#include "pool.h"
#include "gramarray.h"

#ifndef SYMBOL_POOL_SIZE
#define SYMBOL_POOL_SIZE 255
#endif
#if SYMBOL_POOL_SIZE > 255 || SYMBOL_POOL_SIZE <= 0 
#error "SYMBOL_POOL_SIZE should be between 1 and 255"
#endif

#ifndef HENTRY_POOL_SIZE
#define HENTRY_POOL_SIZE 255
#endif
#if HENTRY_POOL_SIZE > 255 || HENTRY_POOL_SIZE <= 0 
#error "HENTRY_POOL_SIZE should be between 1 and 255"
#endif

static const int64_t GRAMARRAY_LARGE = 4611686018427387903LL;

struct symbols;
struct rules;
struct grammars;
struct digram;
struct hentry;

typedef struct symbols 	symbols;
typedef struct rules 	rules;
typedef struct grammars grammars;
typedef struct digram	digram;
typedef struct hentry	hentry;
typedef struct stack	stack;
typedef struct stack_item stack_item;
typedef struct iterator iterator;

typedef enum {
	BACKWARD,
	FORWARD
} direction;

struct rules {
	symbols* 	guard;
	int 		freq;
	int 		index; // for printing
};

struct symbols {
	symbols 	*n, *p;
	intptr_t 	s;
	uint64_t 	i;
};

struct grammars {
	uint64_t 	version;
	rules* 		start;
	symbols* 	root;
	hentry* 	digrams;
	int 		type;
	int64_t		last_input; // only used for RELATIVE types
	rules** all_rules; // used only for printing
	int	num_rules; // number of rules in the grammar
	int 	idx_rules; // index used when printing
};

struct digram {
	intptr_t x, y;
};

struct hentry {
	digram d;
	symbols* s;
	UT_hash_handle hh;
};

struct stack_item {
	uint64_t i;
	symbols* sym;
};

struct stack {
	int top;
	unsigned int size;
	stack_item *content; 
};

struct iterator {
	grammars* grammar;
	uint64_t version;
	stack position;
	int direction;
	int64_t last_item;
};

static pool symbol_pool = { SYMBOL_POOL_SIZE, sizeof(symbols), NULL };
static pool hentry_pool = { HENTRY_POOL_SIZE, sizeof(hentry), NULL };

static hentry*		hentry_alloc();
static void		hentry_free(hentry* entry);

static grammars*	grammar_make(int type);
static void		grammar_free(grammars* g);
static symbols* 	grammar_find_digram(grammars* g, symbols *ss);
static void 		grammar_set_digram(grammars* g, symbols *ss);
static void 		grammar_delete_digram(grammars* g, symbols *ss);
static void 		grammar_append(grammars* g, int64_t x);
static void		grammar_print(grammars* g, FILE* f);

static symbols*		symbol_make_terminal(intptr_t x, int i);
static symbols*		symbol_make_non_terminal(rules* r, int i);
static void		symbol_free(grammars* g, symbols*);
static int		symbol_is_nt(symbols* ss);
static intptr_t		symbol_get_value(symbols* ss);
static rules*		symbol_get_rule(symbols* ss);
static void		symbol_point_to_self(grammars* g, symbols* ss);
static void		symbol_insert_after(grammars* g, symbols* ss, symbols* next);
static void		symbol_join(grammars* g, symbols* left, symbols* right);
static int		symbol_is_guard(symbols* ss);
static void		symbol_substitute(grammars* g, symbols* ss, symbols* b);
static void		symbol_match(grammars* g, symbols* ss, symbols* m);
static int		symbol_check(grammars* g, symbols* ss);
static void		symbol_optimize_iteration(grammars *g, symbols* ss);
static void		symbol_expand(grammars* g, symbols* ss);
static void		symbol_print(symbols* ss, FILE* f);

static rules*		rule_make(grammars* g);
static void		rule_free(grammars* g, rules* r);
static symbols* 	rule_first(rules* r);
static symbols*		rule_last(rules* r);
static void		rule_reuse(rules* r, uint64_t k);
static void 		rule_deuse(rules* r, uint64_t k);
static void		rule_clear(grammars* g, rules* r);
static void		rule_print(grammars* g, rules* r, FILE* f);

static void		stack_init(stack* st, unsigned int size);
static stack_item*	stack_top(stack* st);
static void		stack_pop(stack* st);
static int		stack_push(stack* st, symbols* ss, uint64_t i);
static void		stack_clear(stack* st);

static iterator*	iterator_make(grammars* g, int direction);
static void		iterator_free(iterator* it);
static iterator* 	iterator_copy(iterator* it);
static int		iterator_value(iterator* it, int64_t* v);
static int              iterator_value2(iterator* it, int64_t* v);
static int              iterator_value3(iterator* it, int64_t* v);
static int		iterator_incr(iterator* it);
static int		iterator_incr2(iterator* it);
static int		iterator_decr(iterator* it);
static int		iterator_decr2(iterator* it);

hentry* hentry_alloc()
{
	return pool_alloc_item(&hentry_pool);
}

void hentry_free(hentry* entry)
{
	pool_free_item(&hentry_pool,entry);
}

grammars* grammar_make(int type)
{
	grammars* g = (grammars*)malloc(sizeof(grammars));
	g->version = 1;
	g->digrams = NULL;
	g->start = rule_make(g);
	g->type = type;
	g->last_input = 0;
	g->root  = symbol_make_non_terminal(g->start,1);
	g->all_rules = NULL;
	g->num_rules = 1;
	return g;
}

void grammar_free(grammars* g)
{
	symbol_free(g,g->root);
        g->all_rules = (rules**)malloc(sizeof(rules*)*(g->num_rules));
        g->all_rules[0] = g->start;
        int i = 0;
        g->idx_rules = 1;
        while(i < g->idx_rules) {
                rule_clear(g,g->all_rules[i]);
                i += 1;
        }
        for(i=0; i<g->idx_rules; i++) {
                rule_free(g,g->all_rules[i]);
        }
        free(g->all_rules);
        g->all_rules = NULL;

	{
		hentry *current_entry, *tmp;
		HASH_ITER(hh, g->digrams, current_entry, tmp) {
			HASH_DEL(g->digrams, current_entry);
			free(current_entry);
		}
	}
	free(g);
}

symbols* grammar_find_digram(grammars* g, symbols *ss)
{
	digram d;
	d.x = ss->s;
	d.y = ss->n->s;
	hentry* entry = NULL;
	HASH_FIND(hh,g->digrams,&d,sizeof(digram),entry);
	if(entry == NULL) return NULL;
	return entry->s;
}

void grammar_set_digram(grammars* g, symbols *ss)
{
	if(symbol_is_guard(ss) || symbol_is_guard(ss->n)) return;

//	hentry* entry = (hentry*)malloc(sizeof(hentry));
	hentry* entry = hentry_alloc();
	hentry* replaced = NULL;

	entry->d.x = ss->s;
	entry->d.y = ss->n->s;
	entry->s = ss;

	HASH_REPLACE(hh,g->digrams,d,sizeof(digram),entry,replaced);
	if(replaced && replaced != entry) {
		//free(replaced);
		hentry_free(replaced);
	}
}

void grammar_delete_digram(grammars* g, symbols *ss)
{
	if(symbol_is_guard(ss) || symbol_is_guard(ss->n)) return;

	hentry* entry = NULL;
	digram d;
	d.x = ss->s;
	d.y = ss->n->s;
	HASH_FIND(hh,g->digrams,&d,sizeof(digram),entry);
	if(entry && entry->s == ss) {
		HASH_DELETE(hh,g->digrams,entry);
		//free(entry);
		hentry_free(entry);
	}
}

void grammar_append(grammars* g, int64_t x)
{
	if(g->type == GRAMARRAY_RELATIVE) {
		int64_t y = g->last_input;
		g->last_input = x;
		x -= y;
	}

	// if there will be a capacity overflow,
	// we need to store two symbols
	if(x >= GRAMARRAY_LARGE || x <= -GRAMARRAY_LARGE) {
		uint64_t y = (x > 0 ? GRAMARRAY_LARGE : -GRAMARRAY_LARGE);
		uint64_t i = 1;
		x -= y;
		// deal with a VERY large number (2*GRAMARRAY_LARGE+1 for example)
		if(x >= GRAMARRAY_LARGE || x <= -GRAMARRAY_LARGE) {
			i = 2;
			x -= y;
		}
		symbols* ss = symbol_make_terminal(y,i);
		symbols* last = rule_last(g->start);
		symbol_insert_after(g,last,ss);
		last = rule_last(g->start);
		symbol_check(g,last->p);
	}

	symbols* ss = symbol_make_terminal(x,1);
	symbols* last = rule_last(g->start);
	symbol_insert_after(g,last,ss);

	last = rule_last(g->start);
	symbol_check(g,last->p);
	
	g->version += 1;
}


void grammar_print(grammars* g, FILE* f)
{
	g->all_rules = (rules**)malloc(sizeof(rules*)*(g->num_rules));
	g->all_rules[0] = g->start;
	int i = 0;
	g->idx_rules = 1;
	while(i < g->idx_rules) {
		fprintf(f,"[%d] -> ",i);
		rule_print(g,g->all_rules[i],f);
		fprintf(f,"\n");
		i += 1;
	}
	for(i=0; i<g->idx_rules; i++) {
		g->all_rules[i]->index = 0;
	}
	free(g->all_rules);
	g->all_rules = NULL;
}

symbols* symbol_make_terminal(intptr_t x, int i)
{
	symbols* ss = pool_alloc_item(&symbol_pool);
	ss->i = i;
	ss->s = x < 0 ? 2*x-1 : x*2+1;
	ss->p = ss->n = NULL;
	return ss;
}

symbols* symbol_make_non_terminal(rules* r, int i)
{
	symbols* ss = pool_alloc_item(&symbol_pool);
	ss->i = i;
	ss->s = (intptr_t)r;
	ss->p = ss->n = NULL;
	rule_reuse(r,i);
	return ss;
}

void symbol_free(grammars* g, symbols* ss)
{
	if(!(ss->p == 0 && ss->n == 0)) {
		symbol_join(g,ss->p,ss->n);
		if(!symbol_is_guard(ss)) {
			grammar_delete_digram(g,ss);
			if(symbol_is_nt(ss)) {
				rule_deuse(symbol_get_rule(ss),ss->i);
			}
		}
	}
	pool_free_item(&symbol_pool,ss);
}

int symbol_is_nt(symbols* ss)
{
	return ((ss->s % 2) == 0) && (ss->s != 0);
}

intptr_t symbol_get_value(symbols* ss)
{
	return (ss->s)/2;
}

rules* symbol_get_rule(symbols* ss)
{
	return (rules*)(ss->s);
}

void symbol_point_to_self(grammars* g, symbols* ss)
{
	symbol_join(g,ss,ss);
}

void symbol_insert_after(grammars* g, symbols* ss, symbols* y)
{
	symbol_join(g,y,ss->n);
	symbol_optimize_iteration(g,y);
	symbol_join(g,ss,y);
	symbol_optimize_iteration(g,ss);
}

void symbol_join(grammars* g, symbols* left, symbols* right)
{
	if(left->n && !(symbol_is_guard(left))) {
		grammar_delete_digram(g,left);
		
		if(right->p && right->n 
		&& (right->s == right->p->s)
		&& (right->s == right->n->s))
			grammar_set_digram(g,right);

		if(left->p && left->n
		&& (left->s == left->p->s)
		&& (left->s == left->n->s))
			grammar_set_digram(g,left->p);
	}
	left->n  = right;
	right->p = left;
}

int  symbol_is_guard(symbols* ss)
{
	if(!symbol_is_nt(ss)) return 0;
	rules *r = symbol_get_rule(ss);
	return rule_first(r)->p == ss;
}

void symbol_substitute(grammars* g, symbols* ss, symbols* b)
{
	symbols* q = ss->p;
	symbols* x1 = ss;
	symbols* y1 = x1->n;
	
	uint64_t i1 = x1->i;
	uint64_t j1 = y1->i;
	
	rules* rb = symbol_get_rule(b);
	uint64_t i2 = rule_first(rb)->i;
	uint64_t j2 = rule_first(rb)->n->i;

	if(i1 == i2) {
		symbol_free(g,x1);
	} else {
		x1->i -= i2;
		if(symbol_is_nt(x1)) {
			rule_deuse(symbol_get_rule(x1),i2);
		}
		q = x1;
	}

	if(j1 == j2) {
		symbol_free(g,y1);
	} else {
		y1->i -= j2;
		if(symbol_is_nt(y1)) {
			rule_deuse(symbol_get_rule(y1),j2);
		}
	}

	symbol_insert_after(g,q,b);
	symbol_optimize_iteration(g,q);

	if(!symbol_check(g,q)) {
		symbol_check(g,q->n);
	}

}

void symbol_match(grammars* g, symbols* ss, symbols* m)
{
	rules* r = NULL;
	if(symbol_is_guard(m->p) && symbol_is_guard(m->n->n)
	&& m->i <= ss->i && m->n->i <= ss->n->i) {
		r = symbol_get_rule(m->p);
		symbols* b = symbol_make_non_terminal(r,1);
		symbol_substitute(g,ss,b);
	} else {
		r = rule_make(g);

		uint64_t i = m->i < ss->i ? m->i : ss->i;
		uint64_t j = m->n->i < ss->n->i ? m->n->i : ss->n->i;

		symbols *sc, *snc;
		if(symbol_is_nt(ss))
			sc = symbol_make_non_terminal(symbol_get_rule(ss),i);
		else
			sc = symbol_make_terminal(symbol_get_value(ss),i);

		if(symbol_is_nt(ss->n))
			snc = symbol_make_non_terminal(symbol_get_rule(ss->n),j);
		else
			snc = symbol_make_terminal(symbol_get_value(ss->n),j);

		symbol_insert_after(g,rule_last(r),sc);
		symbol_insert_after(g,rule_last(r),snc);

		symbol_substitute(g,m,symbol_make_non_terminal(r,1));
		symbol_substitute(g,ss,symbol_make_non_terminal(r,1));

		grammar_set_digram(g,rule_first(r));
	}

	symbols* f = rule_first(r);
	if(symbol_is_nt(f)) {
		rules* r2 = symbol_get_rule(f);
		if(r2->freq == 1) {
			symbols* l = rule_last(r2);
			symbol_expand(g,f);
			if(!symbol_check(g,l)) {
				grammar_set_digram(g,l);
			}
		}
	}
}

int symbol_check(grammars* g, symbols* ss)
{
	if(symbol_is_guard(ss) || symbol_is_guard(ss->n)) {
		return 0;
	}
	symbols* x = grammar_find_digram(g,ss);
	if(x == NULL) {
		grammar_set_digram(g,ss);
		return 0;
	}
	if(x != ss && x->n != ss) {
		symbol_match(g,ss,x);
	}
	return 1;
}

void symbol_optimize_iteration(grammars *g, symbols* ss)
{
	int optimized = 0;
	while(!symbol_is_guard(ss->n)) {
		if(ss->s == ss->n->s) {
			grammar_delete_digram(g,ss);
			uint64_t k = ss->n->i;
			ss->i += k;
			if(symbol_is_nt(ss)) {
				rules* r = symbol_get_rule(ss);
				rule_reuse(r,k);
			}
			symbol_free(g,ss->n);
			optimized = 1;
		} else {
			break;
		}
	}
	if(optimized) {
		grammar_set_digram(g,ss);
	}
}

void symbol_expand(grammars* g, symbols* ss)
{
	symbols* left  = ss->p;
	symbols* right = ss->n;
	rules* r = symbol_get_rule(ss);
	symbols* f = rule_first(r);
	symbols* l = rule_last(r);

	grammar_delete_digram(g,ss);

	rule_free(g,r);
	r = NULL;
	ss->s = 0;

	symbol_free(g,ss);
	symbol_join(g,left,f);
	symbol_join(g,l,right);

	symbol_optimize_iteration(g,l);
	if(!symbol_is_guard(left)) {
		symbol_optimize_iteration(g,left);
	}
}

void symbol_print(symbols* ss, FILE* f)
{
	if(symbol_is_nt(ss)) {
		fprintf(f,"[%d]", symbol_get_rule(ss)->index);
	} else {
		fprintf(f,"%ld", (long)symbol_get_value(ss));
	}
	if(ss->i > 1)
		fprintf(f,"^%lu",(unsigned long)(ss->i));
}

rules* rule_make(grammars* g) 
{
	rules* r = (rules*)malloc(sizeof(rules));
	r->freq = 0;
	r->guard = symbol_make_non_terminal(r,1);
	symbol_point_to_self(g,r->guard);
	r->freq = 0;
	g->num_rules += 1;
	r->index = 0;
	return r;
}

void rule_free(grammars* g, rules* r)
{
	symbol_free(g,r->guard);
	free(r);
	g->num_rules -= 1;
}

symbols* rule_first(rules* r)
{
	return r->guard->n;
}

symbols* rule_last(rules* r)
{
	return r->guard->p;
}

void rule_reuse(rules* r, uint64_t k)
{
	r->freq += k;
}

void rule_deuse(rules* r, uint64_t k)
{
	r->freq -= k;
}

void rule_clear(grammars* g, rules* r)
{
	while(r->guard->n != r->guard) {
		symbols* ss = r->guard->n;
		if(symbol_is_nt(ss)) {
			rules* r2 = symbol_get_rule(ss);
			if(r2->index == 0) {
				r2->index = g->idx_rules;
				g->all_rules[r2->index] = r2;
				g->idx_rules += 1;
			}
		}
		symbol_free(g, r->guard->n);
	}
}

void rule_print(grammars* g, rules* r, FILE* f)
{
	symbols* ss = rule_first(r);
	while(!symbol_is_guard(ss)) {
		if(symbol_is_nt(ss)) {
			rules* r2 = symbol_get_rule(ss);
			if(r2->index == 0) {
				r2->index = g->idx_rules;
				g->idx_rules += 1;
				g->all_rules[r2->index] = r2;
			}
		}
		symbol_print(ss,f);
		fprintf(f," ");
		ss = ss->n;
	}
}

void stack_init(stack* st, unsigned int size)
{
	st->content = (stack_item*)malloc(sizeof(stack_item)*size);
	st->top = -1;
	st->size = size;
}

stack_item* stack_top(stack* st)
{
	if(st->top < 0) return NULL;
	return &(st->content[st->top]);
}

void stack_pop(stack* st)
{
	if(st->top >= 0) {
		st->top -= 1;
	}
}

int stack_push(stack* st, symbols* ss, uint64_t i)
{
	if(st->top < (signed)st->size-1) {
		st->top += 1;
		st->content[st->top].i = i;
		st->content[st->top].sym = ss;
		return 0;
	} else {
		return -1;
	}
}

void stack_clear(stack* st)
{
	free(st->content);
	st->top = -1;
	st->size = 0;
}

iterator* iterator_make(grammars* g, int direction)
{
	iterator* it = (iterator*)malloc(sizeof(iterator));
	it->grammar = g;
	it->last_item = direction > 0 ? 0 : g->last_input;
	it->direction = direction;

	stack_init(&(it->position),g->num_rules+1);

	if(g->start->guard->n == g->start->guard) {
		it->version = 0;
		return it;
	}

	it->version = g->version;
	symbols* ss = g->root;
	if(direction > 0) {
		while(symbol_is_nt(ss)) {
			ss = rule_first(symbol_get_rule(ss));
			stack_push(&(it->position),ss,0);
		}
	} else {
		while(symbol_is_nt(ss)) {
			ss = rule_last(symbol_get_rule(ss));
			stack_push(&(it->position),ss,ss->i-1);
		}
		// here we may be pointing to something that has
		// escape values before
		int64_t v = 0;
		// make a copy of the iterator and decrement it
		iterator* itm = iterator_copy(it);
		int r = iterator_decr2(itm);
		if(r) { // cannot decrement, free the copy, return "it"
			iterator_free(itm);
			return it;
		}
		// get the value
		iterator_value3(itm,&v);
		if(v == GRAMARRAY_LARGE || v == -GRAMARRAY_LARGE) {
			// value is an escape value, free it, replace it with itm
			iterator_free(it);
			it = itm;
			itm = iterator_copy(it); // create new copy
			r = iterator_decr2(itm);
			if(r) { // cannot decrement
				iterator_free(itm);
				return it;
			}
			iterator_value3(itm,&v);
			if(v == GRAMARRAY_LARGE || v == -GRAMARRAY_LARGE) {
				iterator_free(it);
				return itm;
			} else {
				iterator_free(itm);
			}
		} else {
			iterator_free(itm);
		}
	}
	return it;
}

void iterator_free(iterator* it)
{
	stack_clear(&(it->position));
	free(it);
}

iterator* iterator_copy(iterator* it)
{
	iterator* it2 = (iterator*)malloc(sizeof(iterator));
	it2->grammar = it->grammar;
	it2->last_item = it->last_item;
	it2->direction = it->direction;
	stack_init(&(it2->position),it->position.size);
	it2->position.top = it->position.top;
	it2->version = it->version;
	memcpy(it2->position.content,it->position.content,it->position.size*sizeof(stack_item));
	return it2;
}

int iterator_value3(iterator* it, int64_t* v)
{
	if(it->grammar->version != it->version) return -1;
	stack_item* si = stack_top(&(it->position));
	if(si == NULL) return -1;
	symbols* ss = si->sym;
	if(ss && !symbol_is_nt(ss)) {
		*v = symbol_get_value(ss);
		return 0;
	} else {
		return -1;
	}
}

int iterator_value2(iterator* it, int64_t* v)
{
	int64_t x,y;
	int r = iterator_value3(it,&x);
	if(r) return r;
	if(x == GRAMARRAY_LARGE || x == -GRAMARRAY_LARGE) {
		iterator* next = iterator_copy(it);
		next->direction = GRAMARRAY_FORWARD;
		r = iterator_incr2(next);
		if(r) return r;
		r = iterator_value3(next,&y);
		if(r) return r;
		*v = x+y;
		// deal wit 2 consecutive escape values
		if(y == GRAMARRAY_LARGE || y == -GRAMARRAY_LARGE) {
			r = iterator_incr2(next);
			if(r) return r;
			r = iterator_value3(next,&y);
			if(r) return r;
			*v += y;
		}
	} else {
		*v = x;
	}
	return 0;
}

int iterator_value(iterator* it, int64_t* v)
{
	if(it->direction < 0) {
		*v = it->last_item;
		return 0;
	}
	int r = iterator_value2(it,v);
	if(r) return r;
	if(it->grammar->type == GRAMARRAY_RELATIVE) {
		*v = *v + it->last_item;
	}
	return 0;
}

int iterator_incr2(iterator* it)
{
	if(it->version != it->grammar->version) return -1;
	if(it->position.top == -1) return -1;

	stack_item* current = stack_top(&(it->position));
	symbols* s = current->sym;
	uint64_t i = current->i;
	stack_pop(&(it->position));
	
	// continue reading an iteration
	if(i < s->i - 1) {
		i += 1;
		stack_push(&(it->position),s,i);
		if(symbol_is_nt(s)) {
			s = rule_first(symbol_get_rule(current->sym));
			stack_push(&(it->position),s,0);
			while(symbol_is_nt(s)) {
				s = rule_first(symbol_get_rule(s));
				stack_push(&(it->position),s,0);
			}
		}
	} else if(!symbol_is_guard(s->n)) {
	// continue reading rule
		symbols* s = current->sym->n;
		stack_push(&(it->position),s,0);
		while(symbol_is_nt(s)) {
			s = rule_first(symbol_get_rule(s));
			stack_push(&(it->position),s,0);
		}
	} else {
	// done reading a rule, s is in the upper rule
		int d = iterator_incr2(it);
		if(d == -1) return -1;
	}

	return 0;
}

int iterator_decr2(iterator* it)
{
	if(it->version != it->grammar->version) return -1;
	if(it->position.top == -1) return -1;

	stack_item* current = stack_top(&(it->position));
	symbols* s = current->sym;
	uint64_t i = current->i;
	stack_pop(&(it->position));

	// reading back an iteration
	if(i > 0) {
		i -= 1;
		stack_push(&(it->position),s,i);
		if(symbol_is_nt(s)) {
			s = rule_last(symbol_get_rule(current->sym));
			uint64_t k = s->i-1;
			stack_push(&(it->position),s,k);
			while(symbol_is_nt(s)) {
				s = rule_last(symbol_get_rule(s));
				uint64_t k = s->i-1;
				stack_push(&(it->position),s,k);
			}
		}
	} else if(!symbol_is_guard(s->p)) {
	// continue reading the rule backward
		symbols* s = current->sym->p;
		uint64_t k = s->i-1;
		stack_push(&(it->position),s,k);
		while(symbol_is_nt(s)) {
			s = rule_last(symbol_get_rule(s));
			uint64_t k = s->i-1;
			stack_push(&(it->position),s,k);
		}
	} else {
	// done reading a rule backward, s is in the upper rule
		int d = iterator_decr2(it);
		if(d == -1) return -1;
	}
	return 0;
}

int iterator_incr(iterator* it)
{
	int64_t v = 0;
	int r = iterator_value3(it,&v);
	if(r) return r;
	if(v == GRAMARRAY_LARGE || v == -GRAMARRAY_LARGE) {
		r = iterator_incr2(it);
		if(r) return r;
		int64_t w;
		r = iterator_value3(it,&w);
		v += w;
		if(r) return r;
		r = iterator_incr2(it);
		if(r) return r;
		// deal with the case of 2 consecutive escape
		if(w == GRAMARRAY_LARGE || w == -GRAMARRAY_LARGE) {
			r = iterator_value3(it,&w);
			v += w;
			if(r) return r;
			r = iterator_incr2(it);
			if(r) return r;
		}
	} else {
		r = iterator_incr2(it);
	}
	if(it->grammar->type == GRAMARRAY_RELATIVE) 
		it->last_item += v;
	else
		it->last_item = v;
	return r;
}

int iterator_decr(iterator* it)
{
	// get the current value pointed to (relative)
	int64_t rel_last;
	int r = iterator_value2(it,&rel_last);
	//fprintf(stderr,"rel_last = %lld\n",rel_last);
	if(r) return r;

	// do one step back
	r = iterator_decr2(it);
	if(r) return r;

	// get the raw value of the last item pointed to
	int64_t v = 0;
	r = iterator_value3(it,&v);
	if(r) return r;

	// copy the iterator and do one step back
	iterator* itm1 = iterator_copy(it);
	int64_t w = 0;
	r = iterator_decr2(itm1);
	if(r) { // cannot decrease further
		goto update_last_item;
	}
	// get previous value
	r = iterator_value3(itm1,&w);
	if(r) return r;

	if(w == GRAMARRAY_LARGE || w == -GRAMARRAY_LARGE) {
		// previous value is an escape value, deal with it
		r = iterator_decr2(it); // make "it" match "itm1"
		if(r) return r;

		v += w; // add the escape value
		r = iterator_decr2(itm1); // can we decrement further?
		if(r) { // cannot decrement further
			goto update_last_item;
		}
		// here we have been able to decrease further
		r = iterator_value3(itm1,&w); // get the value
		if(r) return r;
		if(w == GRAMARRAY_LARGE || w == -GRAMARRAY_LARGE) {
			// again an escape value
			v += w;
			r = iterator_decr2(it); // make "it" match "itm1"
			if(r) return r;
		}
	}

update_last_item:
	if(it->grammar->type == GRAMARRAY_RELATIVE)
		it->last_item -= rel_last;
	else
		it->last_item = v;
	iterator_free(itm1);
	return 0;
}

int gramarray_create(gramarray* g, int type)
{
	*g = (gramarray)grammar_make(type);
	return (*g == 0) ? -1 : 0;
}

int gramarray_free(gramarray g)
{
	if(g == 0) return -1;
	grammar_free((grammars*)g);
	return 0;
}

int gramarray_append(gramarray g, gramtype x)
{
	if(g == 0) return -1;
	grammar_append((grammars*)g,x);
	return 0;
}

int gramarray_iterator_create(gramarray g, gramiter* it, int direction)
{
	if(g == 0) return -1;
	*it = (gramiter)iterator_make((grammars*)g,direction);
	if(*it != 0) return 0;
	else return -1;
}

int gramarray_iterator_free(gramiter it)
{
	if(it == 0) return -1;
	iterator_free((iterator*)it);
	return 0;
}

int gramarray_iterator_value(gramiter it, gramtype* val)
{
	if(it == 0) return -1;
	return iterator_value((iterator*)it, val);
}

int gramarray_iterator_next(gramiter it)
{
	if(it == 0) return -1;
	if(((iterator*)it)->direction > 0)
		return iterator_incr((iterator*)it);
	else
		return iterator_decr((iterator*)it);
}

int gramarray_iterate(gramarray g, gramcallback cb, int direction, void* args)
{
	gramiter it;
	int r;
	r = gramarray_iterator_create(g,&it,direction);
	if(r) goto fn_error;
	do {
		int64_t v;
		r = gramarray_iterator_value(it,&v);
		if(r) goto fn_error;
		cb(v,args);
	} while(gramarray_iterator_next(it) != -1);
	
fn_error:
fn_exit:
	gramarray_iterator_free(it);
	return r;
}

int gramarray_print(FILE* file, gramarray g)
{
	if(g == 0) return -1;
	grammar_print((grammars*)g, file);
	return 0;
}

