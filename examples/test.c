#include <stdio.h>
#include "gramarray.h"

void my_callback(int64_t v, void* args) {
	fprintf(stderr,"%c ",(char)v);
}

#define CHECK(fn) {int _ret; _ret = (fn); if (_ret != 0) fprintf(stderr, "bad return: %s\n", #fn); }

int main() {
	gramarray g;
	CHECK(gramarray_create(&g, GRAMARRAY_RELATIVE));

	int c = 0;
	char* str = "abcdabababcdabababcdabab";

	while(str[c] != 0) {
		CHECK(gramarray_append(g, (gramtype)(str[c])));
		c += 1;
	}
	CHECK(gramarray_print(stdout,g));
	printf("=======\n");

	gramiter it;

	CHECK(gramarray_iterator_create(g, &it, GRAMARRAY_FORWARD));
	fprintf(stderr,"forward: ");
	while(1) {
		int64_t v;
		gramarray_iterator_value(it, &v);
		fprintf(stderr,"%c ",(char)v);
		if(gramarray_iterator_next(it) == -1) break;
	}
	fprintf(stderr,"\n");
	CHECK(gramarray_iterator_free(it));

	fprintf(stderr,"backward: ");
	CHECK(gramarray_iterator_create(g, &it, GRAMARRAY_BACKWARD));
	while(1) {
		int64_t v;
		CHECK(gramarray_iterator_value(it, &v));
		fprintf(stderr,"%c ",(char)v);
		if(gramarray_iterator_next(it) == -1) break;
	}
	fprintf(stderr,"\n");
	CHECK(gramarray_iterator_free(it));

	// same thing with a callback
	fprintf(stderr,"forward: ");  
	CHECK(gramarray_iterate(g,my_callback,GRAMARRAY_FORWARD,NULL));
	fprintf(stderr,"\n");

	fprintf(stderr,"backward: ");
	CHECK(gramarray_iterate(g,my_callback,GRAMARRAY_BACKWARD,NULL));
	fprintf(stderr,"\n");

	CHECK(gramarray_free(g));

	return 0;
}
